import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/app-shared.module';
// c
import { RawMaterialComponent } from './raw-material/raw-material.component';
// s
import { RawMaterialsService } from '../shared-services/raw-materials.service';

const routes: Routes = [
  { path: '', component: RawMaterialComponent }
];

@NgModule({
  declarations: [RawMaterialComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    FormsModule
  ],
  providers: [RawMaterialsService]
})
export class RawMaterialsModule { }
