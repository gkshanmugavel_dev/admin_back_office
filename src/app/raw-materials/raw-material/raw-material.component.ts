import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { CommonService, RawMaterialsService } from 'src/app/shared-services';
import { SnackBarService } from 'src/app/shared-services/snack-bar.service';

@Component({
  selector: 'app-raw-material',
  templateUrl: './raw-material.component.html',
  styleUrls: ['./raw-material.component.scss']
})
export class RawMaterialComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource = new MatTableDataSource<any>([]);
  displayedColumns = ['sNo',
    'date',
    'time',
    'vehicleNumber',
    'itemName',
    'loadedWeight',
    'emptyWeight',
    'netWeight',
    'ton' // have to netWeight / 1000
  ];
  fromDate: Date;
  toDate: Date;
  requestFromDate: string;
  requestToDate: string;
  currentDate = new Date();
  selectedPlant: number;
  totalLength: any;
  offset: any;
  currentLength = 0;
  materialDetails: any = [];
  pagination: any;
  pageSize: any;
  loading: boolean;

  constructor(public common: CommonService,
    private rawMaterialsService: RawMaterialsService,
    private snackBar: SnackBarService) { }

  ngOnInit() {
    const fd = new Date();
    const td = new Date();
    // console.log(d.getDate() - 1);
    this.fromDate = new Date(fd.setDate(td.getDate() - 1));
    this.toDate = td;
    this.dataSource.paginator = this.paginator;
    this.selectedPlant = -1;
    this.currentLength = 0;
    this.getRawMaterials(0);
  }

  getRawMaterials(page) {
    const fMonth = (this.fromDate.getMonth() + 1) < 9 ? '0' + (this.fromDate.getMonth() + 1) : this.fromDate.getMonth() + 1;
    const tMonth = (this.toDate.getMonth() + 1) < 9 ? '0' + (this.toDate.getMonth() + 1) : this.toDate.getMonth() + 1;
    const fDate = this.fromDate.getDate() < 9 ? '0' + this.fromDate.getDate() : this.fromDate.getDate();
    const tDate = this.toDate.getDate() < 9 ? '0' + this.toDate.getDate() : this.toDate.getDate();
    this.requestFromDate = this.fromDate.getFullYear() + '-' + fMonth + '-' + fDate;
    this.requestToDate = this.toDate.getFullYear() + '-' + tMonth + '-' + tDate;
    this.currentLength = page;
    this.loading = true;
    if (page <= 0) {
      this.dataSource = new MatTableDataSource<any>([]);
      this.materialDetails = [];
    }
    this.rawMaterialsService.getMaterials(this.requestFromDate, this.requestToDate, this.selectedPlant, 20, page).subscribe((result) => {
      console.log(result);
      if (result.success) {
        this.materialDetails = this.materialDetails.concat(result.data);
        this.dataSource = result.data;
        this.loading = false;
        // if (!this.pagination || this.pagination.pageIndex > this.pagination.previousPageIndex) {
        //   this.currentLength += 10;
        // }
        this.totalLength = result.count;
      } else {
        this.snackBar.snackbarError(result.message);
      }
    }, err => {
      if (err && err.message) { this.snackBar.snackbarError(err.message); }
    });
  }

  dateChanged(type) {
    this.currentLength = 0;
    this.getRawMaterials(0);
    // if (type === 'from') {
    //   console.log('from date on cghange:', this.fromDate, this.fromDate.getFullYear(), this.fromDate.getMonth() + 1,
    //   this.fromDate.getDate());
    //   this.requestFromDate =  this.fromDate.getFullYear() + '-' + this.fromDate.getMonth() + '-' + this.fromDate.getDate();
    // } else {
    //   console.log('to date on cghange:', this.toDate, this.toDate.getFullYear(), this.toDate.getMonth() + 1, this.toDate.getDate());
    //   this.requestToDate =  this.toDate.getFullYear() + '-' + this.toDate.getMonth() + '-' + this.toDate.getDate();
    // }
  }

  plantChanged() {
    // console.log('selectedPlant:', this.selectedPlant);
    this.currentLength = 0;
    this.getRawMaterials(0);
  }

  pageEvent(event) {
    // console.log(event);
    this.pagination = event;
    // if (event.pageIndex === event.previousPageIndex) {
    //   this.pageSize = event.pageSize;
    //   this.getRawMaterials(this.currentLength);
    //   return;
    // }
    if (event.pageIndex > event.previousPageIndex) {
      this.getRawMaterials(this.currentLength + 20);
    } else {
      this.getRawMaterials(this.currentLength - 20);
    }
  }

}
