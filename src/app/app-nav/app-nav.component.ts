import { ChangeDetectorRef, Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MediaMatcher } from '@angular/cdk/layout';
import { CommonService, LocalStorageService } from '../shared-services';
import { MasterDataService } from '../shared-services/master-data.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-app-nav',
  templateUrl: './app-nav.component.html',
  styleUrls: ['./app-nav.component.scss']
})
export class AppNavComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  selectedTap = '';
  private _mobileQueryListener: () => void;

  constructor(private router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private common: CommonService,
    private storage: LocalStorageService,
    private masterService: MasterDataService) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    // on route change event listener
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((route: NavigationEnd) => {
      console.log(route.url);
      if (route.url === '/orderdetails') {
        this.selectedTap = 'Order Details';
      } else if (route.url === '/rawmaterial') {
        this.selectedTap = 'Raw Materials';
      } else if (route.url === '/plants') {
        this.selectedTap = 'Plants';
      }
    });
  }

  ngOnInit() {
    const plants = this.storage.getLocalStorage('plants');
    if (!plants) {
      this.masterService.getPlants().subscribe((result) => {
        if (result) {
          const plantsData = result.data;
          plantsData.unshift({ id: -1, name: 'Plant All', plantName: 'Plant All' });
          console.log('plantsData', plantsData);
          this.common.plants = plantsData;
          this.storage.setLocalStorage('plants', plantsData);
        }
      }, err => console.log(err));
    } else {
      this.common.plants = plants;
    }
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
