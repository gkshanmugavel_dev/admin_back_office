import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { CommonService } from 'src/app/shared-services';
import { OrderDetailsService } from 'src/app/shared-services/order-details.service';
import { SnackBarService } from 'src/app/shared-services/snack-bar.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss']
})
export class OrderDetailsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns = [
    'orderId',
    'executiveName',
    'Executive_Phone',
    'Customer_Name',
    'Customer_Phone',
    'Customer_Address',
    'Contact_Name',
    'Contact_Phone',
    'Billing_Name',
    'Billing_PhoneNo',
    'Delivery_Address',
    'status'
  ];
  dataSource = new MatTableDataSource<any>([]);
  fromDate: Date;
  toDate: Date;
  requestFromDate: string;
  requestToDate: string;
  currentDate = new Date();
  selectedPlant: number;
  totalLength: any;
  offset: any;
  currentLength = 0;
  orderList: any = [];
  pagination: any;
  pageSize: any;
  loading: boolean;
  exportLoading: boolean;
  constructor(public common: CommonService,
    private orderListService: OrderDetailsService,
    private snackBar: SnackBarService) { }

  ngOnInit() {
    const fd = new Date();
    const td = new Date();
    // console.log(d.getDate() - 1);
    this.fromDate = new Date(fd.setDate(td.getDate() - 1));
    this.toDate = td;
    this.dataSource.paginator = this.paginator;
    this.selectedPlant = -1;
    this.currentLength = 0;
    this.getOrderList(0);
  }

  getOrderList(page) {
    const fMonth = (this.fromDate.getMonth() + 1) < 9 ? '0' + (this.fromDate.getMonth() + 1) : this.fromDate.getMonth() + 1;
    const tMonth = (this.toDate.getMonth() + 1) < 9 ? '0' + (this.toDate.getMonth() + 1) : this.toDate.getMonth() + 1;
    const fDate = this.fromDate.getDate() < 9 ? '0' + this.fromDate.getDate() : this.fromDate.getDate();
    const tDate = this.toDate.getDate() < 9 ? '0' + this.toDate.getDate() : this.toDate.getDate();
    this.requestFromDate = this.fromDate.getFullYear() + '-' + fMonth + '-' + fDate;
    this.requestToDate = this.toDate.getFullYear() + '-' + tMonth + '-' + tDate;
    this.currentLength = page;
    this.loading = true;
    if (page <= 0) {
      this.dataSource = new MatTableDataSource<any>([]);
      this.orderList = [];
    }
    this.orderListService.getOrders(this.requestFromDate, this.requestToDate, this.selectedPlant, 20, page).subscribe((result) => {
      console.log(result);
      if (result.success) {
        this.orderList = this.orderList.concat(result.data);
        this.dataSource = result.data;
        this.loading = false;
        this.totalLength = result.count;
      } else {
        this.snackBar.snackbarError(result.message);
      }
    }, err => {
      if (err && err.message) { this.snackBar.snackbarError(err.message); }
    });
  }

  dateChanged(type) {
    this.currentLength = 0;
    this.getOrderList(0);
  }

  plantChanged() {
    // console.log('selectedPlant:', this.selectedPlant);
    this.currentLength = 0;
    this.getOrderList(0);
  }

  pageEvent(event) {
    // console.log(event);
    this.pagination = event;
    if (event.pageIndex > event.previousPageIndex) {
      this.getOrderList(this.currentLength + 20);
    } else {
      this.getOrderList(this.currentLength - 20);
    }
  }

  export() {
    this.exportLoading = true;
    const fMonth = (this.fromDate.getMonth() + 1) < 9 ? '0' + (this.fromDate.getMonth() + 1) : this.fromDate.getMonth() + 1;
    const tMonth = (this.toDate.getMonth() + 1) < 9 ? '0' + (this.toDate.getMonth() + 1) : this.toDate.getMonth() + 1;
    const fDate = this.fromDate.getDate() < 9 ? '0' + this.fromDate.getDate() : this.fromDate.getDate();
    const tDate = this.toDate.getDate() < 9 ? '0' + this.toDate.getDate() : this.toDate.getDate();
    this.requestFromDate = this.fromDate.getFullYear() + '-' + fMonth + '-' + fDate;
    this.requestToDate = this.toDate.getFullYear() + '-' + tMonth + '-' + tDate;
    this.orderListService.getOrders(this.requestFromDate, this.requestToDate, this.selectedPlant,
      this.totalLength, 0).subscribe((result) => {
        console.log(result);
        this.downloadFile(result.data);
      });
  }

  downloadFile(data: any) {
    const replacer = (key, value) => value === null ? '' : value; // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    const csv = data.map(row => header.map(fieldName => JSON.stringify(row[fieldName], replacer)).join(','));
    csv.unshift(header.join(','));
    const csvArray = csv.join('\r\n');

    const a = document.createElement('a');
    const blob = new Blob([csvArray], { type: 'text/csv' }),
      url = window.URL.createObjectURL(blob);
    this.exportLoading = false;
    a.href = url;
    a.download = 'orderDetails.csv';
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }
}
