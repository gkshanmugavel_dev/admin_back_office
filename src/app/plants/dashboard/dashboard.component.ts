import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { DashboardService } from '../../shared-services/dashboard.service';
import { CommonService } from 'src/app/shared-services';
import { SnackBarService } from 'src/app/shared-services/snack-bar.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns = ['plantName',
    'customerName',
    'invoiceNo',
    'typeOfSand',
    'requiredUnit',
    'vehicleNumber',
    'emptyWeight',
    'loadedWeight',
    'netWeight',
    'actualDeliveryUnit',
    'deliveryAddress',
    'dateTime'
  ];
  dataSource = new MatTableDataSource<any>([]);
  fromDate: Date;
  toDate: Date;
  requestFromDate: string;
  requestToDate: string;
  currentDate = new Date();
  selectedPlant: number;
  totalLength: any;
  offset: any;
  currentLength = 0;
  orderDetails: any = [];
  pagination: any;
  pageSize: any;
  loading: boolean;

  constructor(private dashboardService: DashboardService,
    public common: CommonService,
    private snackBar: SnackBarService) { }

  ngOnInit() {
    const fd = new Date();
    const td = new Date();
    // console.log(d.getDate() - 1);
    this.fromDate = new Date(fd.setDate(td.getDate() - 1));
    this.toDate = td;
    this.dataSource.paginator = this.paginator;
    this.selectedPlant = -1;
    this.currentLength = 0;
    this.getOrderDetails(0);
  }

  getOrderDetails(page) {
    const fMonth = (this.fromDate.getMonth() + 1) < 9 ? '0' + (this.fromDate.getMonth() + 1) : this.fromDate.getMonth() + 1;
    const tMonth = (this.toDate.getMonth() + 1) < 9 ? '0' + (this.toDate.getMonth() + 1) : this.toDate.getMonth() + 1;
    const fDate = this.fromDate.getDate() < 9 ? '0' + this.fromDate.getDate() : this.fromDate.getDate();
    const tDate = this.toDate.getDate() < 9 ? '0' + this.toDate.getDate() : this.toDate.getDate();
    this.requestFromDate = this.fromDate.getFullYear() + '-' + fMonth + '-' + fDate;
    this.requestToDate = this.toDate.getFullYear() + '-' + tMonth + '-' + tDate;
    this.currentLength = page;
    this.loading = true;
    if (page <= 0) {
      this.dataSource = new MatTableDataSource<any>([]);
      this.orderDetails = [];
    }
    this.dashboardService.getOrderDetails(this.requestFromDate, this.requestToDate, this.selectedPlant, 20, page).subscribe((result) => {
        console.log(result);
        if (result.success) {
          this.orderDetails = this.orderDetails.concat(result.data);
          this.dataSource = result.data;
          this.loading = false;
          // if (!this.pagination || this.pagination.pageIndex > this.pagination.previousPageIndex) {
          //   this.currentLength += 10;
          // }
          this.totalLength = result.message;
        } else {
          this.snackBar.snackbarError(result.message);
        }
      }, err => {
        if (err && err.message) { this.snackBar.snackbarError(err.message); }
      });
  }

  dateChanged(type) {
    this.currentLength = 0;
    this.getOrderDetails(0);
    // if (type === 'from') {
    //   console.log('from date on cghange:', this.fromDate, this.fromDate.getFullYear(), this.fromDate.getMonth() + 1,
    //   this.fromDate.getDate());
    //   this.requestFromDate =  this.fromDate.getFullYear() + '-' + this.fromDate.getMonth() + '-' + this.fromDate.getDate();
    // } else {
    //   console.log('to date on cghange:', this.toDate, this.toDate.getFullYear(), this.toDate.getMonth() + 1, this.toDate.getDate());
    //   this.requestToDate =  this.toDate.getFullYear() + '-' + this.toDate.getMonth() + '-' + this.toDate.getDate();
    // }
  }

  plantChanged() {
    // console.log('selectedPlant:', this.selectedPlant);
    this.currentLength = 0;
    this.getOrderDetails(0);
  }

  pageEvent(event) {
    // console.log(event);
    this.pagination = event;
    // if (event.pageIndex === event.previousPageIndex) {
    //   this.pageSize = event.pageSize;
    //   this.getOrderDetails(this.currentLength);
    //   return;
    // }
    if (event.pageIndex > event.previousPageIndex) {
      this.getOrderDetails(this.currentLength + 20);
    } else {
      this.getOrderDetails(this.currentLength - 20);
    }
  }

}
