import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class SnackBarService {

    constructor(private snackBar: MatSnackBar) { }

    snackbarSuccess(message) {
        this.snackBar.open(message, 'ok', {
            duration: environment.snackBarDuration,
            panelClass: ['success-snackbar']
        });
    }

    snackbarError(message) {
        this.snackBar.open(message, 'ok', {
            duration: environment.snackBarDuration,
            panelClass: ['error-snackbar']
        });
    }

    snackbarInfo(msg) {
        this.snackBar.open(msg, 'ok', {
            duration: environment.snackBarDuration,
            panelClass: ['default-snackbar']
        });
    }


}
