import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  login(userName: string, password: string): Observable<any> {
    console.log('calling get order details', userName, password);
    const url = environment.apiUrl + 'user/auth/LoginUserWithId';
    return this.httpClient.post(url, {
        password: password,
        phoneNumber: userName,
        deviceId: environment.deviceId,
        deviceToken: environment.deviceToken
      }
    );
  }
}
