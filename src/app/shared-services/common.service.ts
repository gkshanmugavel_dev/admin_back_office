import { Injectable } from '@angular/core';

@Injectable()
export class CommonService {
    plants: { id: number; name: string; plantName: string }[] = [];
}
