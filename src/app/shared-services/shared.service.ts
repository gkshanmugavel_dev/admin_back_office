import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {

  constructor() { }
}

@Injectable()
export class LocalStorageService {
  getLocalStorage(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  setLocalStorage(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  removeStorage(keys: string[]) {
    keys.forEach(element => {
      localStorage.removeItem(element);
    });
  }

  clearAllStorage() {
    localStorage.clear();
  }
}
