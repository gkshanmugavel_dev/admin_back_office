import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class RawMaterialsService {

  constructor(private httpClient: HttpClient) { }

  getMaterials(fromDate, toDate, plant: number, limit: number, offset: number): Observable<any> {
    console.log('calling get Materials details', fromDate, toDate, limit, offset);
    const url = environment.apiUrl + 'reports/getRawMaterial/' + plant + '?' +
    'fromDate=' + fromDate +
    '&toDate=' + toDate +
    '&limit=' + limit +
    '&offset=' + offset;
    return this.httpClient.get(url);
  }
}
