import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class MasterDataService {

    constructor(private httpClient: HttpClient) { }

    getPlants(): Observable<any> {
        console.log('calling common/getPlantList');
        const url = environment.apiUrl3030 + 'common/getPlantList';
        return this.httpClient.get(url);
    }
}
