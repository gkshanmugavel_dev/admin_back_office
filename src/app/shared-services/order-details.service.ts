import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { LocalStorageService } from '.';

@Injectable()
export class OrderDetailsService {

    constructor(private httpClient: HttpClient,
        private storageService: LocalStorageService) { }

    getOrders(fromDate, toDate, plant: number, limit: number, offset: number): Observable<any> {
        const token = this.storageService.getLocalStorage('token');
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': token
            })
        };
        console.log('calling get order details', fromDate, toDate, limit, offset);
        const url = environment.apiUrl + 'reports/backOffice/getOrderList/' + plant + '?' +
            'fromDate=' + fromDate +
            '&toDate=' + toDate +
            '&limit=' + limit +
            '&offset=' + offset;
        return this.httpClient.get(url, {
            headers: httpOptions.headers
        });
    }

    exportOrders(fromDate, toDate, plant: number, limit: number, offset: number): Observable<any> {
        console.log('calling get order details', fromDate, toDate, limit, offset);
        const url = environment.apiUrl + 'reports/getOrderDetails/' + plant + '?' +
            'fromDate=' + fromDate +
            '&toDate=' + toDate +
            '&limit=' + limit +
            '&offset=' + offset;
        return this.httpClient.get(url);
    }
}
