import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from './';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router,
    private localStorage: LocalStorageService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.localStorage.getLocalStorage('token')) {
      this.router.navigate(['/login']);
      return false;
    } else {
      return true;
    }
  }
}
