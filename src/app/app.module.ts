import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from './shared/app-shared.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthGuardService } from './shared-services/auth-guard.service';
import { LocalStorageService, DashboardService, CommonService } from './shared-services';
import { AppNavComponent } from './app-nav/app-nav.component';
import { OrderDetailsService } from './shared-services/order-details.service';
import { MasterDataService } from './shared-services/master-data.service';
import { SnackBarService } from './shared-services/snack-bar.service';

@NgModule({
  declarations: [
    AppComponent,
    AppNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  providers: [LocalStorageService, AuthGuardService, DashboardService, OrderDetailsService, CommonService, MasterDataService,
    SnackBarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
