import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './shared-services/auth-guard.service';
import { AppNavComponent } from './app-nav/app-nav.component';

const routes: Routes = [
  { path: 'login', loadChildren: './login/login.module#LoginModule' },
  {
    path: '', component: AppNavComponent, canActivate: [AuthGuardService],
    children: [
      { path: '', redirectTo: 'plants', pathMatch: 'full' },
      { path: 'plants', loadChildren: './plants/dashboard.module#DashboardModule' },
      { path: 'rawmaterial', loadChildren: './raw-materials/raw-materials.module#RawMaterialsModule' },
      { path: 'orderdetails', loadChildren: './order-details/order-details.module#OrderDetailsModule' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: false, useHash: true, scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
