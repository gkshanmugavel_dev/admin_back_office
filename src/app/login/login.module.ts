import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/app-shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
// c
import { LoginComponent } from './login/login.component';
// s
import { LoginService } from '../shared-services/login.service';
const routes: Routes = [
  {path: '', component: LoginComponent}
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [LoginService]
})
export class LoginModule { }
