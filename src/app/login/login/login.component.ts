import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService, LoginService } from '../../shared-services';
import { Validators, FormControl } from '@angular/forms';
import { SnackBarService } from 'src/app/shared-services/snack-bar.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginDetail: { name: string; password: string; };
  userName = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);

  constructor(private storage: LocalStorageService,
    private router: Router, private snackBar: SnackBarService,
    private loginService: LoginService) { }

  ngOnInit() {
  }

  getErrorMessage(type) {
    if (type === 'password') {
      return this.userName.hasError('required') ? 'Password required' : '';
    } else {
      return this.password.hasError('required') ? 'Username required' : '';
    }
  }

  login() {
    if (this.userName.valid && this.password.valid) {
      if (this.userName.value === 'admin' && this.password.value === 'admin') {
        this.storage.setLocalStorage('token', 'tokenValue');
        this.router.navigate(['/']);
      } else {
        this.loginService.login(this.userName.value, this.password.value).subscribe((result) => {
          console.log(result.data);
          if (result.success) {
            this.storage.setLocalStorage('token', result.data.accessToken);
            this.storage.setLocalStorage('userData', result.data);
            this.router.navigate(['/']);
          } else {
            this.snackBar.snackbarError(result.message);
          }
        }, err => {
          if (err && err.message) { this.snackBar.snackbarError(err.message); }
        });
      }
    }
  }

}
