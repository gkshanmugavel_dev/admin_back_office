// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://35.244.9.50:8000/api/',
  apiUrl3030: 'http://35.244.9.50:3030/api/',
  deviceId: '866759038538280',
  deviceToken: 'd7tzDFNQ4K8:APA91bFx02nk3zrnINKoRGZAkLkg_DoQtxwVOwJQmocpWFYKTHZSKWeExMKx9dFHVLZDOS6hf3VHKVu41lyLApH7nNmnZ1nZTWHckrKYR75x2LyanpfPXMeq2C4d2z72WduvoTsMulyp',
  snackBarDuration: 3000
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
